var express = require('express')
var router = express.Router()
var path = require('path')

var notes = require(process.env.NOTES_MODEL ? path.join('..', process.env.NOTES_MODEL) : '../models/notes-sequelize')
router.get('/', (req, res, next) => {
  notes.keylist()
  .then(keylist => {
    var keyPromises = []

    for (var key of keylist) {
      keyPromises.push(
        notes.read(key)
        .then(note => {
          return {key: note.key, title: note.title}
        })
      )
    }

    return Promise.all(keyPromises)
  })
  .then(notelist => {
    res.render('index', {title: 'iNotes', notelist: notelist})
  })
  .catch(err => {
    next(err)
  })
})

module.exports = router
