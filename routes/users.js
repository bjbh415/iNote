const express = require('express')
const router = express.Router()
exports.router = router
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const util = require('util')
const path = require('path')
const usersModel = require(process.env.USERS_MODEL? 
    path.join('..',process.env.USERS_MODEL):'../models/users-rest')

exports.initPassport = function(app){
    app.use(passport.initialize())
    app.use(passport.session())
}

exports.ensureAuthenticated = function(req, res, next){
    if(req.user) next()
    else res.redirect('/users/login')
}

//Login
router.get('/login', function(req, res, next){
    util.log(req.user)
    res.render('login', {
        title: 'Login To Notes',
        user : req.user
    })
})

router.post('/login', function(req, res, next){
    passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login'
    })
})

//Logout
router.get('/logout', function(req, res, next){
    req.logout
    res.redirect('/')
})

//Configure Strategy
passport.use(new LocalStrategy(
    function(username, password, done){
        return usersModel.checkPasswordCheck(username, password)
        .then(check=>{
            if(check.check){
                done(null, {id: check.username, username: check.username})
            }else{
                done(null, false, check.message)
            }
            return check
        })
        .catch(err=>done(err))
    }
))

passport.serializeUser(function(username, done){
    done(null, username)
})
passport.deserializeUser(function(username, done){
    usersModel.find(username)
    .then(user=>done(null, user))
    .catch(err => done(err))
})