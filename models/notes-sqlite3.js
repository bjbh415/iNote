'use strict'

const sqlite3 = require('sqlite3').verbose()
const util = require('util')
const Note = require('./Note')

var db;

//connect database
exports.connectDB = function(){
    return new Promise((resolve, reject)=>{
        if(db) return resolve(db)
        var dbfile = process.env.SQLITE_FILE || 'inote.sqlite3';
        db = new sqlite3.Database(dbfile, sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, err => {
            if(err) reject(err)
            resolve(db)
        })
    })
}

//create
exports.create = function(key, title, body){
    return exports.connectDB()
    .then( db => {
        var note = new Note(key, title, body)
        return new Promise((resolve, reject)=>{
            db.run("INSERT INTO notes (notekey, title, body) VALUES(?,?,?);", [
                key,
                title,
                body
            ], err => {
                if(err) reject(err)
                else{
                    resolve(note)
                }
            })
        })
    })
}

//update
exports.update = function(key, title, body){
    return exports.connectDB()
    .then( db => {
        var note = new Note(key, title, body)
        return new Promise((resolve, reject)=>{
            db.run("UPDATE notes SET title = ?, body = ? WHERE notekey = ?;", [
                title,
                body,
                key
            ], err => {
                if(err) reject(err)
                else{
                    console.log('update '+ util.inspect(note));
                    resolve(note)
                }
            })
        })
    })
}

//read
exports.read = function(key){
    return exports.connectDB()
    .then(db=>{
        return new Promise((resolve, reject)=>{
            db.get('SELECT * FROM notes WHERE notekey = ?', [key], (err, row)=>{
                if(err) return reject(err)
                else{
                    console.log(util.inspect(row))
                    var note = new Note(row.notekey, row.title, row.body)
                    resolve(note)
                }
            })
        })
    })
}

//destroy
exports.destroy = function(key){
    return exports.connectDB()
    .then(db=>{
        return new Promise((resolve, reject)=>{
            db.run('DELETE FROM notes WHERE notekey = ?', [key], err=>{
                if(err) return reject(err)
                resolve()
            })
        })
    })
}

//keylist
exports.keylist = function() {
    return exports.connectDB().then((db) => {
        return new Promise((resolve, reject) => {
            var keys = []
            db.each('SELECT notekey FROM notes', (err, row)=>{
                if(err) reject(err)
                else keys.push(row.notekey)
            },(err, num)=>{
                if(err) reject(err)
                else resolve(keys)
            })
        });
    });
};


//count
exports.count = function() {
    return exports.connectDB().then((db) => {
        return new Promise((resolve, reject) => {
            db.get('SELECT count(notekey) as count FROM notes', (err, row)=>{
                if(err) return reject(err)
                resolve(row.count)
            })
        });
    });
};