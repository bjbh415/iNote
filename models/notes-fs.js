'use strict'
var fs = require('fs-extra')
var path = require('path')
var util = require('util')
const Note = require('./Note')

function notesDir () {
  return new Promise((resolve, reject) => {
    const dir = process.env.NOTES_FS_DIR || 'notes-fs-data'
    fs.ensureDir(dir, err => {
      if (err) return reject(err)
      resolve(dir)
    })
  })
}

function filePath (notesDir, key) {
  return path.join(notesDir, key + '.json')
}

function readJSON(notesdir, key) {
    const readFrom = filePath(notesdir, key);
    return new Promise((resolve, reject) => {
        fs.readFile(readFrom, 'utf8', (err, data) => {
            if (err) return reject(err);
            resolve(Note.fromJSON(data));
        });
    });
}

// create & update
module.exports.update = module.exports.create = function (key, title, body) {
  return notesDir()
    .then(notesdir => {
      return new Promise((resolve, reject) => {
        var file = filePath(notesdir, key)
        var data = (new Note(key, title, body)).toJSON
        fs.writeFile(file, data, 'utf8', err => {
          if (err) return reject(err)
          resolve()
        })
      })
    })
}

// read
module.exports.read = function (key) {
   return notesDir().then(notesdir => {
       return readJSON(notesdir, key)
           .then(thenote => {
                return thenote
           })
       })
}
  
// delete
module.exports.destroy = function (key) {
    return notesDir()
        .then(notesdir=>{
            return new Promise((resolve, reject) => {
                var file = filePath(notesdir, key)
                fs.unlink(file, (err)=>{
                    if(err) return reject(err)
                    resolve()
                })
            })
        })
}
module.exports.keylist = function () {
    return notesDir().then(notesdir=>{
        return new Promise((resolve, reject) => {
            fs.readdir(notesdir, 'utf8', (err, files)=>{
                if(err) return reject(err)
                if(!files) files = []
                resolve({notesdir, files})
            })
        })
    }).then(data => {
        var thenotes = data.files.map((fname)=>{
            var key = path.basename(fname, '.json')
            return readJSON(data.notesdir, key).then(thenote => {
                return thenote.key
            })
        })

        return Promise.all(thenotes)
    })
}

module.exports.count = function () {
    return notesDir()
    .then(notesdir => {
        return new Promise((resolve, reject)=>{
            fs.readdir(notesdir, 'utf8', (err, files)=>{
                if(err) return reject(err)
                resolve(files.length)
            })
        })
    })
}
