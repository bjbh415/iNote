'use strict'

var notes = []
const Note = require('./Note')

// create & update
module.exports.update = module.exports.create = function (key, title, body) {
  return new Promise((resolve, reject) => {
    notes[key] = new Note(key, title, body)
    resolve(notes[key])
  })
}

// read
module.exports.read = function (key) {
  return new Promise((resolve, reject) => {
    if (notes[key]) { resolve(notes[key]) } else { return reject(`Note ${key} does not exist`) }
  })
}

// delete
module.exports.destroy = function (key) {
  return new Promise((resolve, reject) => {
    if (notes[key]) {
      delete notes[key]
      resolve()
    } else { return reject(`Note ${key} does not exist`) }
  })
}

module.exports.keylist = function () {
  return new Promise((resolve, reject) => {
    resolve(Object.keys(notes))
  })
}

module.exports.count = function () {
  return new Promise((resolve, reject) => {
    resolve(notes.length)
  })
}
