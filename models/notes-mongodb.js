'use strict'
const mongoClient = require('mongodb').MongoClient
const Note = require('./Note')

const url = 'mongodb://localhost:27017/inote'
var db;

exports.connectDB = function(){
    return new Promise((resolve, reject)=>{
        if(db) return resolve(db)
        mongoClient.connect(process.env.MONGO_SERVER_URL || url, (err, _db)=>{
            if(err) return reject(err)
            db = _db
            resolve(_db)
        })
    })
}
//create
exports.create = function(key, title, body){
    return exports.connectDB()
    .then(db=>{
        var note = new Note(key, title, body);
        var collection = db.collection('notes')
        return collection.insertOne({
            notekey: key,
            title: title,
            body: body
        })
        .then(result => note)
    })
}

//update
exports.update = function(key, title, body){
    return exports.connectDB()
    .then(db=>{
        var note = new Note(key, title, body);
        var collection = db.collection('notes')
        return collection.updataOne({notekey:key}, {$set: {title: title, body: body}})
        .then(result => note)
    })
}

//read

exports.read = function(key){
    return exports.connectDB()
    .then(db=>{
        var collection = db.collection('notes')
        return collection.findOne({notekey: key})
        .then(result=>{
            if(!result) throw new Error('No note found for key: '+key)
            else {
                var note = new Note(result.notekey, result.title, result.body)
                return note
            }
        })
    })
}

//destroy
exports.destroy = function(key){
    return exports.connectDB()
    .then(db=>{
        var collection = db.collection('notes')
        return collection.findOneAndDelete({notekey: key})
    })
}

//keylist
exports.keylist = function(){
    return exports.connectDB()
    .then(db=>{
        var collection = db.collection('notes')
        return collection.find({})
    })
    .then(notes=>{
       return new Promise((resolve, reject)=>{
            var keys=[]
            notes.forEach(
                note=>{ keys.push(note.notekey) },
                err=>{
                    if(err) reject(err)
                    else resolve(keys)
                }
            )
       })
    })
}