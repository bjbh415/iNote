'use strict'

const fs = require('fs-extra')
const Sequelize = require('sequelize')
const jsyaml = require('js-yaml')
const util = require('util')
const Note = require('./Note')


exports.connectDB = function () {
  var SQNote
  var sequlz

  if (SQNote) return SQNote.sync()

  return new Promise((resolve, reject) => {
    fs.readFile(process.env.SEQUELIZE_CONNECT || 'sequelize-sqlite3.yaml', 'utf8', (err, data) => {
      if (err) reject(err)
      else resolve(data)
    })
  }).then(yamltext => {
    return jsyaml.safeLoad(yamltext, 'utf8')
  }).then(params=>{
    sequlz = new Sequelize(params.dbname, params.username, params.password, params.params)
    SQNote = sequlz.define('Notes', {
      notekey:{type:Sequelize.STRING, primaryKey: true, unique: true},
      title: Sequelize.STRING,
      body: Sequelize.TEXT
    })
    return SQNote.sync()
  })
}

//create
exports.create = function(key, title, body){
  return exports.connectDB()
  .then(SQNote=>{
    return SQNote.create({
      notekey: key,
      title: title,
      body: body
    })
  })
}

//update
exports.update = function (key, title, body){
  return exports.connectDB()
  .then(SQNote=>{
    return SQNote.find({where:{notekey: key}})
  })
  .then(note=>{
    if(!note) throw new Error('No note found for key ' + key)
    else return note.updateAttributes({title:title, body: body})
  })
} 

//read
exports.read = function(key){
  return exports.connectDB()
  .then(SQNote=>{
    return SQNote.find({where: {notekey:key}})
  })
  .then(note => {
    if(!note) throw new Error('No note found for key ' + key)
    else return new Note(note.notekey, note.title, note.body)
  })
}

//destroy
exports.destroy = function(key){
  return exports.connectDB()
  .then(SQNote=>{
    return SQNote.find({where: {notekey:key}})
  })
  .then(note=>{
    if(!note) throw new Error('No note found for key ' + key)
    else return note.destroy()
  })
}

//keylist

exports.keylist = function(){
  return exports.connectDB()
  .then(SQNote=>{
    return SQNote.findAll({attributes:['notekey']})
  })
  .then(notes=>{
    return notes.map(note=>note.notekey)  
  })
}

//count

exports.count = function(){
  return exports.connectDB()
  .then(SQNote=>{
    return SQNote.count().then(count => count)
  })
}