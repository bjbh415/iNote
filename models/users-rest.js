'use strict'

const Note = require('./Note')
const restify = require('restify')
const util = require('util')

function connectREST(){
    return new Promise((resolve, reject)=>{
        try{
            resolve(restify.createJsonClient({
                url: process.env.USER_SERVICE_URL,
                version: '*'
            }))
        }catch(err){
            util.log(err)
            reject(err)
        }
    }).then(client=>{
        client.basicAuth('them', 'D4ED43C0-8BD6-4FE2-B358-7C0E230D11EF')
        return client
    })
}

//create
exports.create = function (username, password, provider, 
    familyName, givenName, middleName, emails, photos){
    return connectREST().then(client=>{
        return new Promise((resolve, reject)=>{
            client.post('/create-user', {
                username, password, provider, familyName, 
                givenName, middleName, emails, photos
            }, (err, req, res, obj)=>{
                if(err) return reject(err)
                resolve(obj)
            })
        })
    })
}

//update
exports.update = function (username, password, provider, 
    familyName, givenName, middleName, emails, photos){
    return connectREST().then(client=>{
        return new Promise((resolve, reject)=>{
            client.post('/update-user/'+username, {
                username, password, provider, familyName, 
                givenName, middleName, emails, photos
            }, (err, req, res, obj)=>{
                if(err) return reject(err)
                resolve(obj)
            })
        })
    })
}
//find
exports.find = function(username){
    return connectREST().then(client=>{
        return new Promise((resolve, reject)=>{
            client.get('/find/'+username, (err, req, res, obj)=>{
                if(err)return reject(err)
                resolve(obj)
            })
        })
    })
}

//Password Checking
exports.userPasswordCheck = function(username, password){
    return connectREST().then(client=>{
        return new Promise((resolve, reject)=>{
            client.post('/checkPassword', {username, password}, 
            (err, req, res, obj)=>{
                if(err) return reject(err)
                resolve(obj)
            })
        })
    })
}

exports.findOrCreate = function(profile){
    return connectREST().then(client=>{
        return new Promise((resolve, reject)=>{
            client.post('/find-or-create', {
                id: profile.id, username: profile.username,
                password: profile.password, provider: profile.provider,
                familyName: profile.familyName, givenName: profile.givenName,
                middleName: profile.middleName, emails: profile.emails,
                photos: profile.photos
            },(err, req, res, obj)=>{
                if(err) return reject(err)
                resolve(obj)
            })
        })
    })
}

exports.listUsers = function(){
    return connectREST().then(client=>{
        return new Promise((resolve, reject)=>{
            client.get('/list',(err, req, res, obj)=>{
                if(err) return reject(err)
                resolve(obj)
            })
        })
    })
}